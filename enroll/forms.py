from django.core import validators
from django import forms
from .models import *

class RegistrationForm(forms.ModelForm):
    class Meta:
        model=Registration
        fields = ['name','email','mobile','password']
        widgets = {
            'name':forms.TextInput(attrs={'class':'form-control'}),
            'email':forms.EmailInput(attrs={'class':'form-control'}),
            'mobile':forms.NumberInput(attrs={'class':'form-control'}),
            'password':forms.PasswordInput(render_value=True ,  attrs={'class':'form-control'}),
        }