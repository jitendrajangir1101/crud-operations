from django.shortcuts import render , HttpResponseRedirect
from .forms import RegistrationForm
from .models import Registration

def delete_data(request , id):
    if request.method=='POST':
        pi = Registration.objects.get(pk=id)
        pi.delete()
        return HttpResponseRedirect('/')
def update_data(request , id):
    if request.method == 'POST':
        pi=Registration.objects.get(pk=id)
        fm=RegistrationForm(request.POST ,instance=pi)
        if fm.is_valid():
            fm.save()
            return HttpResponseRedirect('/')
            
    else:
        pi=Registration.objects.get(pk=id)
        fm=RegistrationForm(instance=pi)
    return render(request , 'update.html' ,{'form':fm})




def home(request):
    if request.method == 'POST':
        fm=RegistrationForm(request.POST)
        if fm.is_valid():
            name=fm.cleaned_data['name']
            email=fm.cleaned_data['email']
            mobile=fm.cleaned_data['mobile']
            password=fm.cleaned_data['password']
            reg = Registration(name=name , email=email,mobile=mobile , password=password)
            reg.save()
            fm=RegistrationForm()
    else:
        fm=RegistrationForm()
    stud=Registration.objects.all()
    return render(request, 'show.html',{'form':fm ,'st':stud})

# Create your views here.
