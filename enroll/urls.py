from django.urls import path,include
from . import views

urlpatterns = [
    path('', views.home ,name='show'),
    path('delete/<int:id>/' , views.delete_data , name='delete_data'),
    path('<int:id>/' , views.update_data , name='update_data'),
    
    
]
